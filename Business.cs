using System;
namespace assignment2
{
    public class Business
    {
        private double BuyingPrice, TransportCost, SellingPrice;
        public Business(double DefinedBuyingPrice, double DefinedTransportCost, double DefinedSellingPrice)
        {
            BuyingPrice = DefinedBuyingPrice;
            TransportCost = DefinedTransportCost;
            SellingPrice = DefinedSellingPrice;
        }
        public Business(){
            BuyingPrice = 0.0;
            TransportCost = 0.0;
            SellingPrice = 0.0;
        }

        public void ComputeProfitOrLoss(){
            double Computation = (BuyingPrice + TransportCost);
            if(SellingPrice > Computation){
                Console.WriteLine("Profit Made !");
            }else if(SellingPrice < Computation){
                Console.WriteLine("Loss Recorded");
            }else{
                Console.WriteLine("Neither Profit or Loss Recorded");
            }
        }

    }
}