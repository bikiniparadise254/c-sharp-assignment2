using System;
namespace assignment2
{
    public class Library
    {
        String BookCode, BookName;
        public Library()
        {
        }
        public String Code
        {
            get{ return BookCode; }
            set{ BookCode = value; }
        }
        public String Name
        {
            get{ return BookName; }
            set{ BookName = value; }
        }
    }
}